<?php

namespace app\components;

use Yii;
use yii\base\Component;

abstract class AppManager extends Component
{

	abstract protected function getCurlOpts();

	abstract protected function initUrl($params, $objectUrl);

	abstract protected function generatePostData($data);

	abstract protected function parseResult($result);

	public function makeRequest($objectUrl, $method, $data = '', $params = [])
	{

		$opts = $this->getCurlOpts();

		$ch = curl_init($this->initUrl($params, $objectUrl));
		$this->setHeader($opts, 'Content-Type: application/json');

		switch ($method) {
			case 'GET':
				break;
			case 'POST':
				$postdata = $this->generatePostData($data);
				$opts[CURLOPT_CUSTOMREQUEST] = 'POST';
				$opts[CURLOPT_RETURNTRANSFER] = TRUE;
				$opts[CURLOPT_POSTFIELDS] = $postdata;
				$opts[CURLOPT_POST] = true;
				$this->setHeader($opts, 'Content-Length: ' . strlen($postdata));
				break;
			case 'PUT':
				$postdata = $this->generatePostData($data);
				$opts[CURLOPT_CUSTOMREQUEST] = 'PUT';
				$opts[CURLOPT_RETURNTRANSFER] = TRUE;
				$opts[CURLOPT_POSTFIELDS] = $postdata;
				$this->setHeader($opts, 'Content-Length: ' . strlen($postdata));
				break;
			case 'DELETE':
				$opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
				break;
		}

		curl_setopt_array($ch, $opts);
		$result = curl_exec($ch);

		curl_close($ch);

		return $this->parseResult($result);
	}

	protected function setHeader(&$opts, $headerString = '')
	{
		$opts[CURLOPT_HTTPHEADER][] = $headerString;
	}

}