<?php

namespace app\components;

use Yii;

class QelloManager extends AppManager
{

	public $apiUrl = 'http://api.dev.qkids.com/';

	protected function getCurlOpts()
	{
		return [
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_USERAGENT => 'qello-api-php-beta-0.1',
			CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
			CURLOPT_SSL_VERIFYPEER => false,
		];
	}

	protected function initUrl($params, $objectUrl)
	{
		return $this->apiUrl . $objectUrl . http_build_query($params);
	}

	protected function generatePostData($data)
	{
		$data['device_data'] = [
			'device_name' => Yii::$app->request->userAgent,
			'device_id' => rand(1, 10000000),
			'app_version' => '1.0.0'
		];

		return json_encode($data);
	}

	protected function parseResult($jsonResult)
	{
		return json_decode($jsonResult);
	}

}