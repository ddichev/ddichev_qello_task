<?php
namespace app\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;

class AssetController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index'],
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$userIdentity = Yii::$app->session->get('userIdentity');
		$data = [];
		$total = 0;

		$params = [
			'token' => $userIdentity->accessToken,
			'limit' => 25 * (Yii::$app->request->get('page') ? Yii::$app->request->get('page') : 1),
			'offset' => 25 * (Yii::$app->request->get('page') ? (Yii::$app->request->get('page') - 1) : 0),
			'classification' => 'Episode',
			'type' => 'Video',
		];

		$result = Yii::$app->quelloManager->makeRequest('content?', 'GET', '', $params);

		if($result && isset($result->data->assets, $result->data->total)) {
			$data = $result->data->assets;
			$total = $result->data->total;
		}

		$dataProvider = new ArrayDataProvider([
				'allModels' => $data,
				'pagination' => [
					'pageSize' => 25,
				],
		]);
		$dataProvider->setTotalCount($total);

		return $this->render('index', [
			'dataProvider' => $dataProvider
		]);
	}

}