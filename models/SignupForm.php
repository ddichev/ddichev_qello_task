<?php
namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
	public $first_name;
	public $last_name;
	public $email;
	public $password;
	public $password_repeat;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['email', 'filter', 'filter' => 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],

			['password_repeat', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false, 'message' => "Passwords don't match"],
			[['password', 'password_repeat', 'first_name', 'last_name'], 'required'],
			[['password', 'password_repeat'], 'string', 'min' => 6],
			[['first_name', 'last_name'], 'safe'],
		];
	}

	public function attributeLabels()
	{
		return [
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'password' => 'Password',
			'password_repeat' => 'Repeat Password',
		];
	}

	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function signup()
	{
		if (!$this->validate()) {
			return null;
		}

		$user = new User();
		$user->firstName = $this->first_name;
		$user->lastName = $this->last_name;
		$user->email = $this->email;
		$user->password = $this->password;

		if($user->save()) {
			return $user;
		} else {
			$this->addError('email', $user->errorSummary);
		}

		return null;
	}
}
