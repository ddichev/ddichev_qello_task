<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{

	public $id;
	public $firstName;
	public $lastName;
	public $username;
	public $password;
	public $email;
	public $authKey;
	public $accessToken;
	public $errorSummary;

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return Yii::$app->session->get('userIdentity');
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return null;
	}


	/**
	 * @return $this|bool
	 */
	public function findUser()
	{
		$data = [
				'email' => $this->email,
				'password' => $this->password,
		];

		$result = Yii::$app->quelloManager->makeRequest('users/login', 'POST', $data);

		if (!isset($result->status->success) || $result->status->success != true) {
			$this->errorSummary = isset($result->status->message) ? $result->status->message : '';

			return false;
		}

		$this->id = isset($result->data->id) ? $result->data->id : '';
		$this->accessToken = isset($result->data->token) ? $result->data->token : '';

		Yii::$app->session->set('userIdentity', $this);

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->authKey;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->authKey === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return $this->password === $password;
	}

	public function save($runValidation = true, $attributeNames = NULL)
	{
		$data = [
			'email' => $this->email,
			'password' => $this->password,
			'first_name' => $this->firstName,
			'last_name' => $this->lastName,
		];

		$result = Yii::$app->quelloManager->makeRequest('users', 'POST', $data);

		if (!isset($result->status->success) || $result->status->success != true) {
			$this->errorSummary = isset($result->status->message) ? $result->status->message : '';

			return false;
		}

		$this->id = isset($result->data->id) ? $result->data->id : '';
		$this->accessToken = isset($result->data->token) ? $result->data->token : '';

		Yii::$app->session->set('userIdentity', $this);

		return true;
	}

}
