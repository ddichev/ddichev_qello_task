<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Assets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assets-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Assets Grid</h4>
        </div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => false,
                'filterPosition' => GridView::FILTER_POS_HEADER,
                'options' => ['class' => 'grid table-responsive'],
                'layout' => '{items}{summary}<div class="dataTables_paginate paging_simple_numbers">{pager}</div>',
                'summaryOptions' => array('class' => 'dataTables_info'),
                'pager'=>array('maxButtonCount' => 6),
                'tableOptions' => ['class' => 'table table-striped dataTable no-footer table-filters'],
                'columns' => [
                    [
                        'attribute' => 'id',
                    ],
                    [
                        'attribute' => 'thumbs',
                        'value' => function($model) {
                            return Html::img($model->thumbs, ['style' => 'max-width: 120px;']);
                        },
                        'format' => 'html'
                    ],
                    [
                        'attribute' => 'meta.title',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>